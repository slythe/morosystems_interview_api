package cz.morosystems.slythe.interview.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * {@see https://labs.omniti.com/labs/jsend}
 */
public final class ResponseFactory {

    private ResponseFactory() {
        throw new UnsupportedOperationException();
    }

    public static ResponseEntity ok(@Nullable Object data){
        return success(HttpStatus.OK, data);
    }

    public static ResponseEntity success(HttpStatus status, @Nullable Object data) {
        return from(status, envelope(Status.SUCCESS, null, data));
    }

    public static ResponseEntity fail(HttpStatus status, String message) {
        return from(status, envelope(Status.FAIL, Objects.requireNonNull(message), null));
    }

    public static ResponseEntity error(HttpStatus status, String message) {
        return from(status, envelope(Status.ERROR, Objects.requireNonNull(message), null));
    }

    private static ResponseEntity from(HttpStatus status, Map<String, Object> envelope) {
        return new ResponseEntity<>(envelope, status);
    }

    private static Map<String, Object> envelope(Status status, @Nullable String message, @Nullable Object data) {
        HashMap<String, Object> envelope = new HashMap<>();

        envelope.put("status", status.toString());
        if (message != null) envelope.put("message", message);
        if (data != null) envelope.put("data", data);

        return envelope;
    }

    public enum Status {
        SUCCESS, FAIL, ERROR;

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }
}
