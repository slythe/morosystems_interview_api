package cz.morosystems.slythe.interview.requests;

import javax.validation.constraints.NotBlank;

public class CreateUserRequest {
    @NotBlank
    private String name;

    public String getName() {
        return name;
    }
}
