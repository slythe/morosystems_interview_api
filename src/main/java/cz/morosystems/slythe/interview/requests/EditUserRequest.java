package cz.morosystems.slythe.interview.requests;

import javax.validation.constraints.NotBlank;

public class EditUserRequest {
    @NotBlank
    private String name;

    public String getName() {
        return name;
    }
}
