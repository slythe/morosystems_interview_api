package cz.morosystems.slythe.interview.responses;

import cz.morosystems.slythe.interview.models.UserModel;

public class UserResponse {
    private Long id;
    private String name;

    public UserResponse(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public static UserResponse fromModel(UserModel model) {
        return new UserResponse(model.getId(), model.getName());
    }
}
