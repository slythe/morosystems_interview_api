package cz.morosystems.slythe.interview.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static cz.morosystems.slythe.interview.utils.ResponseFactory.error;
import static cz.morosystems.slythe.interview.utils.ResponseFactory.fail;
import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler({NoHandlerFoundException.class})
    public ResponseEntity handleMethodArgumentNotValid(NoHandlerFoundException e) {
        return fail(NOT_FOUND, message(e));
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity handleMethodArgumentNotValid(EntityNotFoundException e) {
        return fail(BAD_REQUEST, message(e));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleBaseException(Exception e) {
        return error(INTERNAL_SERVER_ERROR, message(e));
    }

    private String message(Exception e) {
        return Optional.ofNullable(e.getMessage())
                .filter(it -> !it.isEmpty())
                .orElse(e.getClass().getSimpleName());
    }
}