package cz.morosystems.slythe.interview.services;

import cz.morosystems.slythe.interview.models.UserModel;
import cz.morosystems.slythe.interview.repositories.UserRepository;
import cz.morosystems.slythe.interview.requests.CreateUserRequest;
import cz.morosystems.slythe.interview.requests.EditUserRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.List;

@Service
public class UserService implements UserDetailsService {
    private UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public List<UserModel> getAll() {
        return this.repository.findAll(Sort.by("id"));
    }

    public void deleteUser(Long id) {
        this.repository.delete(this.repository.findById(id).orElseThrow(EntityNotFoundException::new));
    }

    public UserModel addUser(CreateUserRequest request) {
        UserModel model = new UserModel();
        model.setName(request.getName());

        return this.repository.save(model);
    }

    public UserModel editUser(Long id, EditUserRequest request) {
        UserModel user = this.repository.findById(id).orElseThrow(EntityNotFoundException::new);
        user.setName(request.getName());
        this.repository.save(user);

        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserModel model = this.repository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));

        return new User(model.getUsername(),  model.getPassword(), Collections.emptyList());
    }
}
