package cz.morosystems.slythe.interview.models;

import javax.persistence.*;
import java.time.OffsetDateTime;

@MappedSuperclass
public class BaseModel {
    @Column(nullable = false)
    private OffsetDateTime createdAt;

    @Column(nullable = false)
    private OffsetDateTime updatedAt;

    @PreUpdate
    private void onUpdate() {
        updatedAt = OffsetDateTime.now();
    }

    @PrePersist
    private void onCreate() {
        OffsetDateTime now = OffsetDateTime.now();
        updatedAt = now;
        createdAt = now;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }
}
