package cz.morosystems.slythe.interview.controllers;

import cz.morosystems.slythe.interview.models.UserModel;
import cz.morosystems.slythe.interview.requests.CreateUserRequest;
import cz.morosystems.slythe.interview.requests.EditUserRequest;
import cz.morosystems.slythe.interview.responses.UserResponse;
import cz.morosystems.slythe.interview.services.UserService;
import cz.morosystems.slythe.interview.utils.MapUtils;
import cz.morosystems.slythe.interview.utils.ResponseFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/users/")
public class UserController {
    private UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping("/")
    public ResponseEntity list() {
        List<UserResponse> data = this.service.getAll()
                .stream()
                .map(UserResponse::fromModel)
                .collect(Collectors.toList());

        return ResponseFactory.ok(MapUtils.of("users", data));
    }

    @PostMapping("/")
    public ResponseEntity create(@Valid @RequestBody CreateUserRequest request) {
        UserModel model = this.service.addUser(request);

        return ResponseFactory.success(HttpStatus.CREATED, MapUtils.of("user", UserResponse.fromModel(model)));
    }

    @PatchMapping("/{id}/")
    public ResponseEntity edit(@PathVariable Long id, @Valid @RequestBody EditUserRequest request) {
        UserModel model = this.service.editUser(id, request);

        return ResponseFactory.ok(MapUtils.of("user", UserResponse.fromModel(model)));
    }

    @DeleteMapping("/{id}/")
    public ResponseEntity delete(@PathVariable Long id) {
        this.service.deleteUser(id);

        return ResponseFactory.ok(null);
    }
}
