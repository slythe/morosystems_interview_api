# Interview API

## Requirements
- java 8
- docker
- postgres 10.4

## Installation
- `git clone git@bitbucket.org:slythe/morosystems_interview_api.git`

## Running

### local
- `./gradlew bootRun`

### docker
- `docker-compose up -d` (after first start, you can use `docker-compose start`)
- `docker-compose logs -f` (shows logs from all running containers)

## Configuration
Use `src/main/resources/application-local.properties` for local configuration, make sure to set env variable `SPRING_PROFILES_ACTIVE` to `local` outside docker.

## Links
- https://www.dailycred.com/article/bcrypt-calculator
- https://labs.omniti.com/labs/jsend